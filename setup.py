import os
import numpy as np

try:
    from setuptools import setup
    from setuptools import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension

from Cython.Build import cythonize

apidir = 'C:/GAMS/win64/31.1/apifiles/C/api'

ext_modules = [Extension(
    "gdxr.gdxcy",
    ["cython/gdxcy.pyx", os.path.join(apidir, "gdxcc.c")],
    include_dirs=[apidir, np.get_include()]
)]

setup(
    name='gdx-reader',
    version='0.2.7',
    packages=['gdxr'],
    ext_modules=cythonize(
        ext_modules,
        compiler_directives={'language_level' : "3"}
        ),
    url='https://bitbucket.org/jonull/gdx-reader/src/main/',
    author='Jonathan Ullmark',
    author_email='jonathan.ullmark@chalmers.se',
    license='GPLv3',
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
